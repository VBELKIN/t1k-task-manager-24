package ru.t1k.vbelkin.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public final String NAME = "about";

    @NotNull
    public final String ARGUMENT = "-a";

    @NotNull
    public final String DESCRIPTION = "Display developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[About]");
        System.out.println("Name: Vadim Belkin");
        System.out.println("E-mail: vbelkin@tsconsulting.ru");
        System.out.println("Web-Site: https://gitlab.com/VBELKIN");
    }

}
