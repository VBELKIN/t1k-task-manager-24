package ru.t1k.vbelkin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.api.service.IUserService;
import ru.t1k.vbelkin.tm.command.AbstractCommand;
import ru.t1k.vbelkin.tm.exception.entity.UserNotFoundException;
import ru.t1k.vbelkin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
