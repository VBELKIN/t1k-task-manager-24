package ru.t1k.vbelkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Display program arguments.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final AbstractCommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
