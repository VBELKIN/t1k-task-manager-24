package ru.t1k.vbelkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.enumerated.Role;
import ru.t1k.vbelkin.tm.model.User;

public interface IAuthService {
    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    void checkRoles(@Nullable Role[] roles);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    User getUser();
}
