package ru.t1k.vbelkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    void add(@Nullable AbstractCommand command);

    AbstractCommand getCommandByArgument(@Nullable String argument);

    AbstractCommand getCommandByName(@Nullable String name);

}
