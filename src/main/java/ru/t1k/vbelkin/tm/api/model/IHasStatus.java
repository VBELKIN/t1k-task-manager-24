package ru.t1k.vbelkin.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.t1k.vbelkin.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
